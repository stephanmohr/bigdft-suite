
        #This Makefile is automatically generated from the BigDFT installer to avoid the calling to
        #the installer again for future action on this build
        #Clearly such actions only _assume_ that the build is fully functional and almost nothing
        #can be done with this file if a problem might arise.
        #Otherwise stated: this is an automatic message, please do not reply.
all: build
        
startover:  
	/home/stephan/Documents/BigDFT/devbranch/Installer.py startover spred -y
dist:  
	/home/stephan/Documents/BigDFT/devbranch/Installer.py dist spred -y
autogen:  
	/home/stephan/Documents/BigDFT/devbranch/Installer.py autogen spred -y
cleanone:  
	/home/stephan/Documents/BigDFT/devbranch/Installer.py cleanone spred -y
update:  
	/home/stephan/Documents/BigDFT/devbranch/Installer.py update spred -y
buildone:  
	/home/stephan/Documents/BigDFT/devbranch/Installer.py buildone spred -y
check:  
	/home/stephan/Documents/BigDFT/devbranch/Installer.py check spred -y
dry_run:  
	/home/stephan/Documents/BigDFT/devbranch/Installer.py dry_run spred -y
make:  
	/home/stephan/Documents/BigDFT/devbranch/Installer.py make spred -y
build:  
	/home/stephan/Documents/BigDFT/devbranch/Installer.py build spred -y
clean:  
	/home/stephan/Documents/BigDFT/devbranch/Installer.py clean spred -y
link:  
	/home/stephan/Documents/BigDFT/devbranch/Installer.py link spred -y
